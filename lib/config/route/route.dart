
import 'package:flutter_challenge_jurisvassaivandro/scene/auth/login_screen.dart';
import 'package:flutter_challenge_jurisvassaivandro/scene/home/home_screen.dart';
import 'package:flutter_challenge_jurisvassaivandro/scene/splash/splash_screen.dart';

final routes = {
  '/': (context) =>const SplashScreen(),
  '/login': (context) =>const LoginPage(),
  '/home': (context) =>const HomePage(),
};