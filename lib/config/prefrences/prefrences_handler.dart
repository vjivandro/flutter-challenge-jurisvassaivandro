import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constant.dart';

class PrefrencesHanlder{
  late SharedPreferences pref;
  late bool isLoggedin;
  late String statusLogin = "statuslogin";
  late int id;
  late String username, firstname, lastname, gender, image, token;
  
  void check_isLogin() async {
    pref = await SharedPreferences.getInstance();
    isLoggedin = (pref.getBool(statusLogin) ?? false);
    id = (pref.getInt(TAG_ID) ?? 0);
    username = (pref.getString(TAG_USERNAME) ?? "");
    firstname = (pref.getString(TAG_FIRSTNAMA) ?? "");
    lastname = (pref.getString(TAG_LASTNAME) ?? "");
    gender = (pref.getString(TAG_GENDER) ?? "");
    image = (pref.getString(TAG_IMAGE) ?? "");
    token = (pref.getString(TAG_TOKEN) ?? "");

    if (isLoggedin) {
      Get.offNamed('/home');
    } else {
      Get.offNamed('/login');
    }
  }

  void store_data() async {
    pref = await SharedPreferences.getInstance();
    pref.setBool(statusLogin, true);
    pref.setInt(TAG_ID, id);
    pref.setString(TAG_USERNAME, username);
    pref.setString(TAG_FIRSTNAMA, firstname);
    pref.setString(TAG_LASTNAME, lastname);
    pref.setString(TAG_GENDER, gender);
    pref.setString(TAG_IMAGE, image);
    pref.setString(TAG_TOKEN, token);
    Get.offNamed('/home');
  }

  void logout() async {
    pref = await SharedPreferences.getInstance();
    pref.setBool(statusLogin, false);
    pref.remove(TAG_TOKEN);
    Get.offAllNamed('/login');
  }
}