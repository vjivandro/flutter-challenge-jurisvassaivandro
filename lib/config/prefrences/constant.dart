const String TAG_ID = "id";
const String TAG_USERNAME = "username";
const String TAG_FIRSTNAMA = "firstname";
const String TAG_LASTNAME = "lastname";
const String TAG_GENDER = "gender";
const String TAG_IMAGE = "image";
const String TAG_TOKEN = "token";