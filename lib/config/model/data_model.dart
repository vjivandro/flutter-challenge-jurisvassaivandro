class DataModel {
  final int id;
  final String title;
  final String description;
  final int price;
  final int stock;
  final String brand;
  final String rating;
  final String diskon;
  final String category;
  final String thumbnail;

  const DataModel({
    required this.id,
    required this.title,
    required this.description,
    required this.price,
    required this.stock,
    required this.brand,
    required this.rating,
    required this.diskon,
    required this.category,
    required this.thumbnail
  });

  factory DataModel.fromJson(Map<String, dynamic> json) {
    return DataModel(
      id: json['id'],
      title: json['title'],
      description: json['description'],
      price: json['price'],
      rating: json['rating'],
      diskon: json['discountPercentage'],
      stock: json['stock'],
      brand: json['brand'],
      category: json['category'],
      thumbnail: json['thumbnail'],
    );
  }
}