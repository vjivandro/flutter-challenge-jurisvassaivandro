class ProductModel {
  final List<dynamic> products;

  const ProductModel({
    required this.products,
  });

  factory ProductModel.fromJson(Map<String, dynamic> json) {
    return ProductModel(
      products: json['products'],
    );
  }
}