import 'package:flutter/material.dart';

class LoginBackground extends StatelessWidget {
  final Widget child;

  const LoginBackground({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: double.infinity,
      height: size.height,
      child: Stack(
        alignment: Alignment.center,
        fit: StackFit.loose,
        children: [
          Positioned(
            top: 0,
            left: 0,
            child: Image.asset(
                "assets/header-login.png",
            ),
          ),
          // Positioned(
          //   top: 55,
          //   child: Image.asset(
          //       "assets/logo.png",
          //       width: size.width * 0.30
          //   ),
          // ),
          child
        ],
      ),
    );
  }
}