import 'package:flutter/material.dart';
import 'package:flutter_challenge_jurisvassaivandro/config/model/data_model.dart';
import 'package:flutter_challenge_jurisvassaivandro/scene/home/product_handler.dart';
import 'package:flutter_challenge_jurisvassaivandro/theme.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../config/prefrences/prefrences_handler.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _pref = PrefrencesHanlder();
  final _product = ProductHandler();
  late Future<List<DataModel>> futureProduct;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    futureProduct = _product.getAllProduct();
  }

  Future<void> showLogoutDialog(String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: Text(message),
          ),
          actions: [
            TextButton(
              child: const Text('Logout'),
              onPressed: () {
                _pref.logout();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
        actions: [
          IconButton(
              onPressed: () {
                showLogoutDialog(
                    "Logout alert", "Anda yakin ingin keluar dari aplikasi?");
              },
              icon: const Icon(Icons.logout_rounded))
        ],
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              flex: 8,
              child: FutureBuilder<List<DataModel>>(
                future: _product.getAllProduct(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return GridView.builder(
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 2 / 3,
                        ),
                        itemCount: snapshot.data!.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            child: Card(
                              clipBehavior: Clip.antiAlias,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Image.network(snapshot.data![index].thumbnail,
                                      width: size.width * 0.7,
                                      height: size.height * 0.2,
                                      fit: BoxFit.fill),
                                  const Expanded(
                                    child: SizedBox(),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          snapshot.data![index].title,
                                          style: heading6,
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 10),
                                          child: Text(
                                            snapshot.data![index].brand,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 8,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Row(
                                              children: [
                                                const Icon(
                                                  FontAwesomeIcons.star,
                                                  color: Colors.orangeAccent,
                                                  size: 16.0,
                                                ),
                                                const SizedBox(
                                                  width: 8.0,
                                                ),
                                                Text(
                                                  "${snapshot.data![index].rating}",
                                                  textAlign: TextAlign.center,
                                                ),
                                              ],
                                            ),
                                            Spacer(),
                                            Row(
                                              children: [
                                                const Icon(
                                                  FontAwesomeIcons.boxArchive,
                                                  color: Colors.orangeAccent,
                                                  size: 16.0,
                                                ),
                                                const SizedBox(
                                                  width: 8.0,
                                                ),
                                                Text(
                                                  "${snapshot.data![index].stock}",
                                                  textAlign: TextAlign.center,
                                                ),
                                              ],
                                            ),
                                            Spacer(),
                                            Row(
                                              children: [
                                                const Icon(
                                                  FontAwesomeIcons.percent,
                                                  color: Colors.orangeAccent,
                                                  size: 16.0,
                                                ),
                                                const SizedBox(
                                                  width: 8.0,
                                                ),
                                                Text(
                                                  "${snapshot.data![index].diskon}",
                                                  textAlign: TextAlign.center,
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  const Expanded(
                                    child: SizedBox(),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                                    child: Row(
                                      children: [
                                        Text(
                                          "\$ ${snapshot.data![index].price}",
                                          style: const TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.green),
                                          textAlign: TextAlign.start,
                                        ),
                                        Spacer(),
                                        const Icon(
                                          FontAwesomeIcons.cartShopping,
                                          color: Colors.orangeAccent,
                                          size: 16.0,
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        });
                  } else if (snapshot.hasError) {
                    return Text("data error");
                  }

                  return const CircularProgressIndicator();
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
