import 'package:flutter_challenge_jurisvassaivandro/config/api/api_provider.dart';
import 'package:flutter_challenge_jurisvassaivandro/config/model/data_model.dart';

class ProductHandler {
  Future<List<DataModel>> getAllProduct() async {
    final response = await ApiProvider().getMethod('products');
    List<dynamic> list = response['products'];

    late List<DataModel> listData = <DataModel>[];
    for (var i = 0; i < list.length; i++) {
      int id = list[i]['id'];
      String title = list[i]['title'];
      String description = list[i]['description'];
      int price = list[i]['price'];
      int stock = list[i]['stock'];
      String brand = list[i]['brand'];
      String rating = "${list[i]['rating']}";
      String diskon = "${list[i]['discountPercentage']}";
      String category = list[i]['category'];
      String thumbnail = list[i]['thumbnail'];
      listData.add(DataModel(
          id: id,
          title: title,
          description: description,
          price: price,
          stock: stock,
          brand: brand,
          rating: rating,
          diskon: diskon,
          category: category,
          thumbnail: thumbnail));
    }

    return listData;
  }
}
