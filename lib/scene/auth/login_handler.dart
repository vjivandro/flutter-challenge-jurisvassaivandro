import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_challenge_jurisvassaivandro/config/api/api_provider.dart';
import 'package:flutter_challenge_jurisvassaivandro/config/prefrences/prefrences_handler.dart';
import 'package:http/http.dart';

import '../../config/request/user.dart';

class LoginHandler {
  final _pref = PrefrencesHanlder();
  late int id;
  late String username, firstname, lastname, gender, image, token;

  void login(BuildContext context,String userid, String password, bool isLoading) async {
    User user = User(userid, password);
    Map<String, dynamic> map = {
      'username': user.username,
      'password': user.password
    };

    String streams = jsonEncode(map);

    final response = await ApiProvider().postMethod('auth', 'login', streams);

    id = response['id'];
    username = response['username'];
    firstname = response['firstName'];
    lastname = response['lastName'];
    gender = response['gender'];
    image = response['image'];
    token = response['token'];

    showLoginDialog(context, "Selamat", "Anda berhasil login");
  }

  Future<void> showLoginDialog(BuildContext context,String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: Text(message),
          ),
          actions: [
            TextButton(
              child: const Text('Ok'),
              onPressed: () {
                _pref.isLoggedin = true;
                _pref.id = id;
                _pref.username = username;
                _pref.firstname = firstname;
                _pref.lastname = lastname;
                _pref.gender = gender;
                _pref.image = image;
                _pref.token = token;
                _pref.store_data();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

}