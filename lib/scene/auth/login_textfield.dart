import 'package:flutter/material.dart';

class TextFieldLogin extends StatelessWidget {
  const TextFieldLogin({
    Key? key,
    required this.size,
    required this.hintText,
    required this.obscureText,
    required this.icoton,
    required this.controller,
  }) : super(key: key);

  final Size size;
  final String hintText;
  final bool obscureText;
  final IconButton icoton;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(horizontal: 40),
      height: 50.0,
      width: size.width*0.8,
      child: TextField(
        controller: controller,
        obscureText: obscureText,
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: TextStyle(color: Colors.black.withOpacity(0.45)),
          suffix: icoton,
        ),
      ),
    );
  }
}
