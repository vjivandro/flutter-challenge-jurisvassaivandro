import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../components/background_login.dart';
import '../../components/component_alertdialog.dart';
import '../../theme.dart';
import 'button_login.dart';
import 'login_handler.dart';
import 'login_textfield.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final loginHandler = LoginHandler();
  late bool passenable = true;
  late bool isLoading = false;

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: LoginBackground(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: size.width * 0.1,),
                Image.asset(
                    "assets/logo.png",
                    width: size.width * 0.30
                ),

                Expanded(
                  child: SizedBox(),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.symmetric(horizontal: 40),
                  child: const Text(
                    "Login",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                    textAlign: TextAlign.left,
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.symmetric(horizontal: 40),
                  child: const Text(
                    "Please sign in to continue.",
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.left,
                  ),
                ),

                SizedBox(height: size.height * 0.03),
                TextFieldLogin(
                  size: size,
                  controller: _usernameController,
                  hintText: "User ID",
                  obscureText: false,
                  icoton: IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      FontAwesomeIcons.eye,
                      color: Colors.transparent,
                    ),
                  ),
                ),

                SizedBox(height: size.height * 0.03),
                TextFieldLogin(
                    size: size,
                    controller: _passwordController,
                    hintText: "Password",
                    obscureText: passenable,
                    icoton: IconButton(
                        onPressed: () {
                          setState(() {
                            if (passenable) {
                              passenable = false;
                            } else {
                              passenable = true;
                            }
                          });
                        },
                        icon: Icon(passenable == true
                            ? FontAwesomeIcons.eyeSlash
                            : FontAwesomeIcons.eye))),

                // button
                SizedBox(height: size.height * 0.05),
                ButtonLogin(
                  size: size,
                  press: () {
                    if (isLoading) return;
                    setState(() {
                      isLoading = true;
                    });



                    if (_usernameController.text.isEmpty) {
                      isLoading = false;
                      showMyDialog(context, "Oopss", "User ID dan atau Password anda belum diisi.");
                    } else if (_passwordController.text.isEmpty) {
                      isLoading = false;
                      showMyDialog(context, "Oopss", "User ID dan atau Password anda belum diisi.");
                    } else {
                      loginHandler.login(context, _usernameController.text,
                          _passwordController.text, isLoading);
                    }
                  },
                  child: (isLoading)
                      ? const SizedBox(
                      width: 16,
                      height: 16,
                      child: CircularProgressIndicator(
                        color: Colors.white,
                        strokeWidth: 1.5,
                      ))
                      : const Text(
                    "LOGIN",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),

                const Expanded(
                  child: SizedBox(),
                ),

                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.symmetric(vertical: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Don't have an account? ",
                        style: regular16pt.copyWith(color: textGrey),
                      ),
                      GestureDetector(
                        onTap: () {
                          Get.toNamed("/register");
                        },
                        child: Text(
                          'Sign Up',
                          style: regular16pt.copyWith(color: textOrange),
                        ),
                      ),
                    ],
                  ),
                ),

              ],
            ),
          )),
    );
  }
}
