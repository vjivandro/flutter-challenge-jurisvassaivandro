import 'package:flutter/material.dart';

import '../../theme.dart';

class ButtonLogin extends StatelessWidget {
  const ButtonLogin(
      {Key? key, required this.size, required this.press, this.child})
      : super(key: key);

  final Size size;
  final Function() press;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      margin: const EdgeInsets.symmetric(horizontal: 40),
      width: size.width * 0.7,
      child: MaterialButton(
        onPressed: press,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
        textColor: Colors.white,
        padding: const EdgeInsets.all(0),
        child: Container(
          alignment: Alignment.center,
          height: 50.0,
          width: size.width * 0.4,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(80.0),
              color: textViolet),
          padding: const EdgeInsets.all(0),
          child: child,
        ),
      ),
    );
  }
}
