import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_challenge_jurisvassaivandro/components/background_splash.dart';
import 'package:flutter_challenge_jurisvassaivandro/config/prefrences/prefrences_handler.dart';
import 'package:get/get.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final _pref = PrefrencesHanlder();
  @override
  void initState() {
    super.initState();

    Timer(
        const Duration(seconds: 2),
        // () => Get.offNamed('/login')
        () => _pref.check_isLogin()
    );
  }

  void check_login() {
    Get.offNamed('/login');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SplashBackground(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

          ],
        ),
      ),
    );
  }
}
